/*@{*/// header
#pragma once
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <lemon/concepts/digraph.h>
#include <lemon/concepts/graph.h>
#include <lemon/concepts/maps.h>
#include <lemon/list_graph.h>
using std::string;
/*@}*/

namespace lemon {
typedef ListGraph Graph;
typedef Graph::Node Node;
typedef Graph::Edge Edge;

typedef Graph::NodeIt NodeIt;
typedef Graph::NodeMap<bool> CutMap;
typedef Graph::NodeMap<int> NodeColorMap;
typedef Graph::NodeMap<int> NodeIndexMap;
typedef Graph::NodeMap<int> NodeIntMap;
typedef Graph::NodeMap<bool> NodeBoolMap;
typedef Graph::NodeMap<string> NodeStringMap;
typedef Graph::NodeMap<double> NodePosMap;
typedef Graph::NodeMap<Node> NodeNodeMap;
typedef Graph::NodeMap<Edge> NodeEdgeMap;

typedef Graph::EdgeIt EdgeIt;
typedef Graph::IncEdgeIt IncEdgeIt;
typedef Graph::EdgeMap<double> EdgeValueMap;
typedef Graph::EdgeMap<int> EdgeColorMap;
typedef Graph::EdgeMap<int> EdgeIndexMap;
typedef Graph::EdgeMap<int> EdgeIntMap;
typedef Graph::EdgeMap<string> EdgeStringMap;
typedef Graph::EdgeMap<Edge> EdgeEdgeMap;
typedef Graph::EdgeMap<Node> EdgeNodeMap;
typedef Graph::EdgeMap<bool> EdgeBoolMap;
typedef Preflow<Graph, EdgeValueMap> PFType;

typedef ListDigraph Digraph;
typedef Digraph::Arc Arc;
typedef Digraph::ArcIt ArcIt;
typedef Digraph::InArcIt InArcIt;
typedef Digraph::OutArcIt OutArcIt;
typedef Digraph::ArcMap<double> ArcValueMap;
typedef Digraph::ArcMap<string> ArcStringMap;
typedef Digraph::ArcMap<int> ArcColorMap;
typedef Digraph::ArcMap<string> ArcStringMap;
typedef Digraph::ArcMap<bool> ArcBoolMap;

typedef Digraph::Node Dinode;
typedef Digraph::NodeIt DinodeIt;
typedef Digraph::NodeMap<int> DinodeIntMap;
typedef Digraph::NodeMap<double> DinodeValueMap;
typedef Digraph::NodeMap<int> DinodeColorMap;
typedef Digraph::NodeMap<double> DinodePosMap;
typedef Digraph::NodeMap<string> DinodeStringMap;
typedef Digraph::NodeMap<bool> DinodeBoolMap;
typedef Digraph::NodeMap<Arc> DinodeArcMap;
}

# README

`lemonade` is a c++ library, projected by Flávio Keidi Miyazawa and Mauro Henrique Mulati, that is a __lemon aid__, ie, it helps the usage of the [lemon c++ library](http://lemon.cs.elte.hu). For now, `lemonade` is a header-only library, however, this probably will change.